# Rust C Compiler

A C compiler, written in... Rust!

Why? You may ask. Well I wanted to write a compiler. And I wanted to learn Rust.
So why not mix the two?

And here we are.

# Building
 - Clone the Repo
 - Make sure you've `apt install build-essential`ed
 - Run `cargo build` in the folder.
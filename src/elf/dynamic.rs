pub struct Elf64Dyn {
    d_tag: super::Elf64Sxword,
    d_un: DUn,
}

pub enum DUn {
    DVal(super::Elf64Xword),
    Dptr(super::Elf64Addr),
}

mod program;
mod section;

pub use self::program::Elf64Phdr;
pub use self::section::Elf64Shdr;
pub use self::section::Elf64ShdrType;
use serialise::Serialisable;
use std::mem::transmute;

// const EI_NIDENT: u8 = 16;
const ELFCLASS64: u8 = 0x02;
const ELFDATA2LSB: u8 = 0x01;
const ELFOSABI_NONE: u8 = 0x00;

const EI_MAG0: u8 = 0x7f;
const EI_MAG1: u8 = 0x45; /*E*/
const EI_MAG2: u8 = 0x4c; /*L*/
const EI_MAG3: u8 = 0x46; /*F*/
const EI_CLASS: u8 = ELFCLASS64;
const EI_DATA: u8 = ELFDATA2LSB;
const EI_VERSION: u8 = 0x01;
const EI_OSABI: u8 = ELFOSABI_NONE;
// const EI_PAD: [u8; 8] = [0x00; 8];

// const ET_REL: super::Elf64_Half = 0x0100;
const ET_EXEC: super::Elf64Half = 2;
const EM_X86_64: super::Elf64Half = 62;
const E_VERSION: super::Elf64Word = 1;
// const E_ENTRY_REL: super::Elf64_Addr = 0x0000000000000000; /* DEFAULT Value */
const E_ENTRY_EXEC: super::Elf64Addr = 4194480; /* DEFAULT Value */
// const E_PHOFF_REL: super::Elf64_Off = 0x0000000000000000; /* DEFAULT Value */
const E_PHOFF_EXEC: super::Elf64Off = 64; /* EXEC specific */
// const E_SHOFF_REL: super::Elf64_Off = 0x4000000000000000; /* DEFAULT Value */
const E_SHOFF_EXEC: super::Elf64Off = 592; /* EXEC specific */
const E_FLAGS: super::Elf64Word = 0;
const E_EHSIZE: super::Elf64Half = 64;
// const E_PHENTSIZE_REL: super::Elf64_Half = 0x0000; /* DEFAULT Value */
const E_PHENTSIZE_EXEC: super::Elf64Half = 56; /* EXEC specific */
// const E_PHNUM_REL: super::Elf64_Half = 0x0000; /* DEFAULT Value */
const E_PHNUM_EXEC: super::Elf64Half = 2; /* EXEC specific */
const E_SHENTSIZE: super::Elf64Half = 64; /* EXEC specific */
const E_SHNUM: super::Elf64Half = 6; /* EXEC specific */
const E_SHSTRNDX: super::Elf64Half = 3; /* EXEC specific */

#[derive(Debug, Clone)]
#[repr(C)]
pub struct Elf64EHdr {
    e_ident: [u8; 16],
    e_type: u16,
    e_machine: u16,
    e_version: u32,
    e_entry: u64, //super::Elf64Addr,
    e_phoff: u64, //super::Elf64Off,
    e_shoff: u64, //super::Elf64Off,
    e_flags: u32,
    e_ehsize: u16,
    e_phentsize: u16,
    e_phnum: u16,
    e_shentsize: u16,
    e_shnum: u16,
    e_shstrndx: u16,
}

impl Elf64EHdr {
    pub fn new() -> Elf64EHdr {
        Elf64EHdr {
            // TODO: put in EI_MAG, EI_CLASS etc values
            e_ident: generate_e_ident(),
            e_type: ET_EXEC,
            e_machine: EM_X86_64,
            e_version: E_VERSION,
            e_entry: E_ENTRY_EXEC,
            e_phoff: E_PHOFF_EXEC,
            e_shoff: E_SHOFF_EXEC,
            e_flags: E_FLAGS,
            e_ehsize: E_EHSIZE,
            e_phentsize: E_PHENTSIZE_EXEC,
            e_phnum: E_PHNUM_EXEC,
            e_shentsize: E_SHENTSIZE,
            e_shnum: E_SHNUM,
            e_shstrndx: E_SHSTRNDX,
        }
    }
}

fn generate_e_ident() -> [u8; 16] {
    let mut e_ident: [u8; 16] = [0x00; 16];

    e_ident[0] = EI_MAG0;
    e_ident[1] = EI_MAG1;
    e_ident[2] = EI_MAG2;
    e_ident[3] = EI_MAG3;
    e_ident[4] = EI_CLASS;
    e_ident[5] = EI_DATA;
    e_ident[6] = EI_VERSION;
    e_ident[7] = EI_OSABI;

    for i in 8..15 {
        e_ident[i] = 0x00;
    }

    e_ident
}

impl Serialisable for Elf64EHdr {
    fn to_bytes(self) -> Vec<u8> {
        unsafe {
            let bytes: [u8; 64] = transmute(self);
            bytes.to_vec()
        }
    }
}

#[cfg(test)]
mod tests {
    use elf::Elf64EHdr;
    use serialise::Serialisable;
    #[test]
    fn header_serialises() {
        let expected: Vec<u8> = vec![
            0x7f, 0x45, 0x4c, 0x46, 0x02, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x02, 0x00, 0x3e, 0x00, 0x01, 0x00, 0x00, 0x00, 0xb0, 0x00, 0x40, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x50, 0x02,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x38, 0x00,
            0x02, 0x00, 0x40, 0x00, 0x06, 0x00, 0x03, 0x00,
        ];
        let header = Elf64EHdr::new();
        assert_eq!(expected, header.to_bytes());
    }
}

use elf::{Elf64Addr, Elf64Off};
use serialise::Serialisable;
use std::mem::transmute;

#[repr(C)]
pub struct Elf64Phdr {
    p_type: u32,
    p_flags: u32,
    p_offset: Elf64Off,
    p_vaddr: Elf64Addr,
    p_paddr: Elf64Addr,
    p_filesz: u64,
    p_memsz: u64,
    p_align: u64,
}

impl Elf64Phdr {
    pub fn text() -> Elf64Phdr {
        Elf64Phdr {
            p_type: 1,
            p_flags: 5,
            p_offset: 0,
            p_vaddr: 4194304,
            p_paddr: 4194304,
            p_filesz: 215,
            p_memsz: 215,
            p_align: 2097152,
        }
    }

    pub fn data() -> Elf64Phdr {
        Elf64Phdr {
            p_type: 1,
            p_flags: 6,
            p_offset: 216,
            p_vaddr: 6291672,
            p_paddr: 6291672,
            p_filesz: 13,
            p_memsz: 13,
            p_align: 2097152,
        }
    }
}

impl Serialisable for Elf64Phdr {
    fn to_bytes(self) -> Vec<u8> {
        unsafe {
            let bytes: [u8; 56] = transmute(self);
            bytes.to_vec()
        }
    }
}

#[cfg(test)]
mod tests {
    use elf::header::program::Elf64Phdr;
    use serialise::Serialisable;

    #[test]
    fn text_program_header_serialises() {
        let expected: Vec<u8> = vec![
            0x01, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00,
            0x00, 0x00, 0x00, 0x00, 0xd7, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xd7, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00,
        ];
        let text_program_header = Elf64Phdr::text();
        assert_eq!(expected, text_program_header.to_bytes());
    }

    #[test]
    fn data_program_header_serialises() {
        let expected: Vec<u8> = vec![
            0x01, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0xd8, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0xd8, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0xd8, 0x00, 0x60, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00,
        ];
        let text_program_header = Elf64Phdr::data();
        assert_eq!(expected, text_program_header.to_bytes());
    }
}

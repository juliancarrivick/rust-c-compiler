mod dynamic;
mod header;
mod relocation;

use elf::header::{Elf64EHdr, Elf64Phdr, Elf64Shdr, Elf64ShdrType};
use serialise::Serialisable;
use std::vec::Vec;

pub type Elf64Addr = u64;
pub type Elf64Off = u64;
pub type Elf64Section = u16;
pub type Elf64Versym = u16;
pub type ElfByte = u8;
pub type Elf64Half = u16;
pub type Elf64Sword = i32;
pub type Elf64Word = u32;
pub type Elf64Sxword = i64;
pub type Elf64Xword = u64;

#[repr(C)]
pub struct Elf64 {
    e_header: Elf64EHdr,
    e_phdrs: Vec<Elf64Phdr>,
    e_sections: Vec<Elf64ShdrType>,
    e_shdrs: Vec<Elf64Shdr>,
}

impl Elf64 {
    pub fn new() -> Elf64 {
        Elf64 {
            e_header: Elf64EHdr::new(),
            e_phdrs: vec![Elf64Phdr::text(), Elf64Phdr::data()],
            e_sections: vec![
                Elf64ShdrType::text(),
                Elf64ShdrType::data(),
                Elf64ShdrType::shstrtab(),
                Elf64ShdrType::symtab(),
                Elf64ShdrType::strtab(),
            ],
            e_shdrs: vec![
                Elf64Shdr::null(),
                Elf64Shdr::text(),
                Elf64Shdr::data(),
                Elf64Shdr::shstrtab(),
                Elf64Shdr::symtab(),
                Elf64Shdr::strtab(),
            ],
        }
    }
}

impl Serialisable for Elf64 {
    fn to_bytes(self) -> Vec<u8> {
        let mut bytes: Vec<u8> = Vec::new();
        bytes.extend(&self.e_header.to_bytes());

        for phdr in self.e_phdrs {
            bytes.extend(&phdr.to_bytes());
        }

        for section in self.e_sections {
            bytes.extend(&section.to_bytes());
        }

        for shdr in self.e_shdrs {
            bytes.extend(&shdr.to_bytes());
        }

        bytes
    }
}

pub struct Elf64Rel {
    r_offset: super::Elf64Off,
    r_info: u64,
}

pub struct Elf64Rela {
    r_offset: super::Elf64Off,
    r_info: u64,
    r_addend: i64,
}

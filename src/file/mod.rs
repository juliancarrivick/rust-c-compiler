use std::fs::File;
use std::io::prelude::*;
use std::io::Result;
use std::path::Path;

pub fn get_file_contents(file_name: &str) -> Result<String> {
    let path = Path::new(&file_name);
    let mut file = File::open(path)?;
    let mut contents = String::new();

    match file.read_to_string(&mut contents) {
        Ok(_) => Ok(contents),
        Err(e) => Err(e),
    }
}

pub fn write_buffer_to_file(buf: &Vec<u8>, file_name: &str) -> Result<()> {
    let mut buffer = File::create(file_name)?;
    buffer.write(buf)?;

    Ok(())
}

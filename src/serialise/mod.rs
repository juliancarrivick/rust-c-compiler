pub trait Serialisable {
    fn to_bytes(self) -> Vec<u8>;
}

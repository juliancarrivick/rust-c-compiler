use std::fmt;

#[derive(Debug, PartialEq)]
pub enum Instruction {
    Movl(LongOperand, LongOperand),
    Addl(LongOperand, LongOperand),
    Subl(LongOperand, LongOperand),
    IMul(LongOperand, LongOperand),
    IDiv(LongOperand),
    Cdq,
}

#[derive(Debug, PartialEq)]
pub enum LongOperand {
    Value(i32),
    Eax,
    Ebx,
}

impl fmt::Display for Instruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Instruction::Movl(src, dest) => write!(f, "movl {} {}", src, dest),
            Instruction::Addl(src, dest) => write!(f, "addl {} {}", src, dest),
            Instruction::Subl(src, dest) => write!(f, "subl {} {}", src, dest),
            Instruction::IMul(src, dest) => write!(f, "imul {} {}", src, dest),
            Instruction::IDiv(divisor) => write!(f, "idiv {}", divisor),
            Instruction::Cdq => write!(f, "cdq"),
        }
    }
}

impl fmt::Display for LongOperand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            LongOperand::Value(v) => write!(f, "${}", v),
            LongOperand::Eax => write!(f, "%eax"),
            LongOperand::Ebx => write!(f, "%ebx"),
        }
    }
}

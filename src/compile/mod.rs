mod instruction;

use compile::instruction::{Instruction, LongOperand};
use elf;
use file;
use parse::ast::{Ast, Expression};
use serialise::Serialisable;
use std::io::Result;

pub fn file(file_name: &str) -> Result<()> {
    let file = file::get_file_contents(file_name);

    match file {
        Ok(f) => println!("{}: {}", file_name, f),
        Err(e) => panic!("Opening {} caused error: {}", file_name, e),
    };

    let elf = elf::Elf64::new();
    let elf_bytes = elf.to_bytes();
    let out_file_name = format!("{}.out", file_name);
    file::write_buffer_to_file(&elf_bytes, &out_file_name)
}

pub fn to_assembly(ast: &Ast) -> Vec<Instruction> {
    ast.expression.to_instructions()
}

impl Expression {
    fn to_instructions(&self) -> Vec<Instruction> {
        self.load(LongOperand::Eax)
    }

    fn load(&self, operand: LongOperand) -> Vec<Instruction> {
        match self {
            Expression::Int32(v) => vec![Instruction::Movl(LongOperand::Value(*v), operand)],
            Expression::Add(left, right) => {
                let mut i = Vec::new();
                i.append(&mut left.load(LongOperand::Eax));
                i.append(&mut right.load(LongOperand::Ebx));
                i.push(Instruction::Addl(LongOperand::Ebx, LongOperand::Eax));
                i
            }
            Expression::Subtract(left, right) => {
                let mut i = Vec::new();
                i.append(&mut left.load(LongOperand::Eax));
                i.append(&mut right.load(LongOperand::Ebx));
                i.push(Instruction::Subl(LongOperand::Ebx, LongOperand::Eax));
                i
            }
            Expression::Multiply(left, right) => {
                let mut i = Vec::new();
                i.append(&mut left.load(LongOperand::Eax));
                i.append(&mut right.load(LongOperand::Ebx));
                i.push(Instruction::IMul(LongOperand::Ebx, LongOperand::Eax));
                i
            }
            Expression::Divide(left, right) => {
                let mut i = Vec::new();
                i.append(&mut left.load(LongOperand::Eax));
                i.append(&mut right.load(LongOperand::Ebx));
                i.push(Instruction::Cdq);
                i.push(Instruction::IDiv(LongOperand::Ebx));
                i
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn loads_int32() {
        let instructions = Expression::Int32(5).to_instructions();

        let expected = vec![Instruction::Movl(LongOperand::Value(5), LongOperand::Eax)];
        assert_eq!(expected, instructions);
    }

    #[test]
    fn loads_add() {
        let input = Expression::Add(
            Box::new(Expression::Int32(2)),
            Box::new(Expression::Int32(5)),
        );
        let instructions = input.to_instructions();

        let expected = vec![
            Instruction::Movl(LongOperand::Value(2), LongOperand::Eax),
            Instruction::Movl(LongOperand::Value(5), LongOperand::Ebx),
            Instruction::Addl(LongOperand::Ebx, LongOperand::Eax),
        ];
        assert_eq!(expected, instructions);
    }

    #[test]
    fn loads_sub() {
        let input = Expression::Subtract(
            Box::new(Expression::Int32(2)),
            Box::new(Expression::Int32(5)),
        );
        let instructions = input.to_instructions();

        let expected = vec![
            Instruction::Movl(LongOperand::Value(2), LongOperand::Eax),
            Instruction::Movl(LongOperand::Value(5), LongOperand::Ebx),
            Instruction::Subl(LongOperand::Ebx, LongOperand::Eax),
        ];
        assert_eq!(expected, instructions);
    }

    #[test]
    fn loads_mul() {
        let input = Expression::Multiply(
            Box::new(Expression::Int32(2)),
            Box::new(Expression::Int32(5)),
        );
        let instructions = input.to_instructions();

        let expected = vec![
            Instruction::Movl(LongOperand::Value(2), LongOperand::Eax),
            Instruction::Movl(LongOperand::Value(5), LongOperand::Ebx),
            Instruction::IMul(LongOperand::Ebx, LongOperand::Eax),
        ];
        assert_eq!(expected, instructions);
    }

    #[test]
    fn loads_div() {
        let input = Expression::Divide(
            Box::new(Expression::Int32(2)),
            Box::new(Expression::Int32(5)),
        );
        let instructions = input.to_instructions();

        let expected = vec![
            Instruction::Movl(LongOperand::Value(2), LongOperand::Eax),
            Instruction::Movl(LongOperand::Value(5), LongOperand::Ebx),
            Instruction::Cdq,
            Instruction::IDiv(LongOperand::Ebx),
        ];
        assert_eq!(expected, instructions);
    }
}

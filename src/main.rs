#![allow(dead_code)]

mod args;
mod compile;
mod elf;
mod file;
mod parse;
mod serialise;

use std::io::Result;

fn main() {
    let compilation_filenames = args::get_arguments();

    if compilation_filenames.len() == 0 {
        println!("No filenames were passed");
    }

    for file_name in compilation_filenames {
        let result = compile_file(&file_name);

        match result {
            Ok(_) => println!("Wrote Elf to {}.out successfully", file_name),
            Err(e) => println!("Unable to write elf file: {}", e),
        };
    }
}

fn compile_file(file_name: &str) -> Result<()> {
    let contents = file::get_file_contents(&file_name)?;
    let ast = parse::generate_ast(&contents);
    let instructions = compile::to_assembly(&ast.unwrap());

    for instruction in instructions {
        println!("{}", instruction);
    }

    Ok(())
}

pub mod ast;
mod token;

use parse::ast::*;
use parse::token::*;
use std::collections::VecDeque;
use std::iter::Peekable;
use std::str::Chars;
use std::vec::Vec;

pub fn generate_ast(file_contents: &str) -> Option<Ast> {
    if !file_contents.is_ascii() {
        // TODO Err("Not ASCII")
        return Option::None;
    }

    let mut chars = file_contents.chars().peekable();
    let tokens = tokenise_text(&mut chars);
    let postfix_tokens = convert_to_postfix(&tokens);
    let root_expression = generate_expression_tree(&postfix_tokens);

    Option::Some(Ast {
        expression: root_expression,
    })
}

fn tokenise_text(text: &mut Peekable<Chars>) -> Vec<Token> {
    let mut tokens = Vec::new();

    let mut token = next_token(text);
    while token.is_some() {
        tokens.push(token.unwrap());
        token = next_token(text);
    }
    tokens
}

fn next_token(text: &mut Peekable<Chars>) -> Option<Token> {
    while text.peek()?.is_whitespace() {
        text.next();
    }

    return Token::consume(text);
}

fn convert_to_postfix(tokens: &Vec<Token>) -> Vec<Token> {
    let mut postfix = VecDeque::new();
    let mut operation = VecDeque::new();

    for token in tokens {
        match token {
            Token::Value(v) => postfix.push_back(Token::Value(*v)),
            Token::Operation(op) => {
                // TODO check precendence
                if let Some(Token::Operation(_)) = operation.back() {
                    postfix.push_back(operation.pop_back().unwrap());
                }

                operation.push_back(Token::Operation(*op))
            }
            Token::BeginExpression => operation.push_back(Token::BeginExpression),
            Token::EndExpression => {
                match operation.pop_back() {
                    Some(Token::Operation(o)) => postfix.push_back(Token::Operation(o)),
                    Some(t) => panic!("Unexpected token when ending expression {:?}", t),
                    None => panic!("No operation to pop"),
                };
                match operation.pop_back() {
                    Some(Token::BeginExpression) => {}
                    Some(t) => panic!("Unexpected token {:?}", t),
                    None => panic!("Expected closing bracket"),
                };
            }
            _ => panic!("Unexpected token when converting to postfix"),
        };
    }

    while let Some(token) = operation.pop_back() {
        postfix.push_back(token);
    }

    Vec::from(postfix)
}

fn generate_expression_tree(postfix_tokens: &Vec<Token>) -> Expression {
    let mut expressions: VecDeque<Expression> = VecDeque::new();
    for token in postfix_tokens {
        if let Token::Value(v) = token {
            expressions.push_back(Expression::Int32(*v));
            continue;
        }

        // Right first to preserve original order
        // e.g. 3 / 4 -> 3 4 /
        let right = Box::new(expressions.pop_back().unwrap());
        let left = Box::new(expressions.pop_back().unwrap());
        let expression = match token {
            Token::Value(v) => Expression::Int32(*v),
            Token::Operation(Op::Add) => Expression::Add(left, right),
            Token::Operation(Op::Subtract) => Expression::Subtract(left, right),
            Token::Operation(Op::Multiply) => Expression::Multiply(left, right),
            Token::Operation(Op::Divide) => Expression::Divide(left, right),
            Token::BeginExpression | Token::EndExpression => {
                panic!("Begin and End expression not expected")
            }
            _ => panic!("Unexpected token"),
        };
        expressions.push_back(expression);
    }

    expressions.pop_back().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_tokenise_text() {
        let text = "((5*3) + 2) / 4";
        let mut chars = text.chars().peekable();
        let tokens = tokenise_text(&mut chars);

        let expected = vec![
            Token::BeginExpression,
            Token::BeginExpression,
            Token::Value(5),
            Token::Operation(Op::Multiply),
            Token::Value(3),
            Token::EndExpression,
            Token::Operation(Op::Add),
            Token::Value(2),
            Token::EndExpression,
            Token::Operation(Op::Divide),
            Token::Value(4),
        ];
        assert_eq!(expected, tokens);
    }

    #[test]
    fn test_convert_to_postfix() {
        let input = vec![
            Token::BeginExpression,
            Token::BeginExpression,
            Token::Value(5),
            Token::Operation(Op::Multiply),
            Token::Value(3),
            Token::EndExpression,
            Token::Operation(Op::Add),
            Token::Value(2),
            Token::EndExpression,
            Token::Operation(Op::Divide),
            Token::Value(4),
        ];

        let postfix = convert_to_postfix(&input);

        let expected = vec![
            Token::Value(5),
            Token::Value(3),
            Token::Operation(Op::Multiply),
            Token::Value(2),
            Token::Operation(Op::Add),
            Token::Value(4),
            Token::Operation(Op::Divide),
        ];
        assert_eq!(expected, postfix);
    }
}

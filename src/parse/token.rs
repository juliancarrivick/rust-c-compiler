use std::iter::Peekable;
use std::str::Chars;

#[derive(Debug, Clone, PartialEq)]
pub enum Token {
    Value(i32),
    Operation(Op),
    BeginExpression,
    EndExpression,
    Type(Type),
    Variable(String),
    Call(InbuiltFn),
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Op {
    Add,
    Subtract,
    Multiply,
    Divide,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Type {
    Int,
}

#[derive(Debug, Clone, PartialEq)]
pub enum InbuiltFn {
    Print,
}

impl Token {
    pub fn consume(chars: &mut Peekable<Chars>) -> Option<Token> {
        if let Some(value) = Token::to_value(chars) {
            return Some(value);
        } else if let Some(expression) = Token::to_expression(chars) {
            return Some(expression);
        } else if let Some(type_or_variable) = Token::to_type_or_variable(chars) {
            return Some(type_or_variable);
        }

        None
    }

    fn to_value(chars: &mut Peekable<Chars>) -> Option<Token> {
        if !chars.peek()?.is_ascii_digit() {
            return None;
        }

        let mut value = String::new();
        while chars.peek().is_some() && chars.peek().unwrap().is_ascii_digit() {
            value.push(chars.next().unwrap());
        }

        let val = value.parse::<i32>();
        match val {
            Ok(v) => Some(Token::Value(v)),
            _ => None,
        }
    }

    fn to_expression(chars: &mut Peekable<Chars>) -> Option<Token> {
        let expression_token = match chars.peek() {
            Some('(') => Some(Token::BeginExpression),
            Some(')') => Some(Token::EndExpression),
            Some('+') => Some(Token::Operation(Op::Add)),
            Some('-') => Some(Token::Operation(Op::Subtract)),
            Some('*') => Some(Token::Operation(Op::Multiply)),
            Some('/') => Some(Token::Operation(Op::Divide)),
            Some(_) => None,
            None => None,
        };

        if expression_token.is_some() {
            chars.next();
        }

        expression_token
    }

    fn to_type_or_variable(chars: &mut Peekable<Chars>) -> Option<Token> {
        let mut value = String::new();
        while chars.peek().is_some() && chars.peek().unwrap().is_ascii_alphabetic() {
            value.push(chars.next().unwrap());
        }

        match value.as_str() {
            "int" => Some(Token::Type(Type::Int)),
            "print" => Some(Token::Call(InbuiltFn::Print)),
            _ => Some(Token::Variable(value)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_consume_value() {
        let text = "4";
        let mut chars = text.chars().peekable();
        let token = Token::consume(&mut chars);

        assert_eq!(Token::Value(4), token.unwrap());
    }

    #[test]
    fn test_consume_type() {
        let text = "int";
        let mut chars = text.chars().peekable();
        let token = Token::consume(&mut chars);

        assert_eq!(Token::Type(Type::Int), token.unwrap());
    }

    #[test]
    fn test_consume_variable() {
        let text = "integer";
        let mut chars = text.chars().peekable();
        let token = Token::consume(&mut chars);

        assert_eq!(Token::Variable("integer".to_string()), token.unwrap());
    }

    #[test]
    fn test_consume_call() {
        let text = "print";
        let mut chars = text.chars().peekable();
        let token = Token::consume(&mut chars);

        assert_eq!(Token::Call(InbuiltFn::Print), token.unwrap());
    }
}

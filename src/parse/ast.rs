#[derive(Debug)]
pub struct Ast {
    pub expression: Expression,
}

#[derive(Debug)]
pub enum Statement {
    Declaration(String),
    Assignment(String, Expression),
    Print(String),
}

#[derive(Debug)]
pub enum Expression {
    Int32(i32),
    Add(Box<Expression>, Box<Expression>),
    Subtract(Box<Expression>, Box<Expression>),
    Multiply(Box<Expression>, Box<Expression>),
    Divide(Box<Expression>, Box<Expression>),
}
